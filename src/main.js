var board = {
  w: undefined,
  h: undefined
}

var title = {
  a: 'group',
  b: '180'
}

var b1, b2, c1, c2
var mainColor, background
var randomNums = []

// constants
const Y_AXIS = 1
const X_AXIS = 2

var sampleVinyl = function() {
  this.bgColor = [128, 128, 128]
  this.gradientState = false
  this.gradientA = [0, 0, 0]
  this.gradientB = [255, 255, 255]
  this.noiseAmount = 0.0
  this.noiseColor = [0, 0, 0]
  this.mainColor = [255, 255, 255]
  this.stepNum = 250
  this.wobbliness = 0
  this.groupY = 0
  this.groupH = 0
  this.g180Y = 0
  this.g180H = 0
}

window.onload = function() {
  // set dat.gui
  vinyl = new sampleVinyl()
  setValue()
  var gui = new dat.GUI()
  gui.addColor(vinyl, 'bgColor').onChange(setValue)
  gui.add(vinyl, 'gradientState').onChange(setValue)
  gui.addColor(vinyl, 'gradientA').onChange(setValue)
  gui.addColor(vinyl, 'gradientB').onChange(setValue)
  gui.add(vinyl, 'noiseAmount', 0, 0.75, 0.001).onChange(setValue)
  gui.addColor(vinyl, 'noiseColor').onChange(setValue)
  gui.addColor(vinyl, 'mainColor').onChange(setValue)
  gui.add(vinyl, 'stepNum', 1, 250, 1).onChange(setValue)
  gui.add(vinyl, 'wobbliness', 0, 1, 0.001).onChange(setValue)
  gui.add(vinyl, 'groupY', -1, 1, 0.001).onChange(setValue)
  gui.add(vinyl, 'groupH', -1, 1, 0.001).onChange(setValue)
  gui.add(vinyl, 'g180Y', -1, 1, 0.001).onChange(setValue)
  gui.add(vinyl, 'g180H', -1, 1, 0.001).onChange(setValue)
}

function setup() {
  createCanvas(windowWidth, windowHeight)

  // define vinyl size
  if (windowWidth >= windowHeight) {
    board.h = windowHeight * 0.95
    board.w = board.h
  } else {
    board.w = windowWidth * 0.95
    board.h = board.w
  }

  // define colors
  b1 = color(255)
  b2 = color(255)
}

function setValue() {
  c1 = color(vinyl.gradientA[0], vinyl.gradientA[1], vinyl.gradientA[2])
  c2 = color(vinyl.gradientB[0], vinyl.gradientB[1], vinyl.gradientB[2])
}

function draw() {
  background(0)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  textAlign(CENTER, CENTER)
  imageMode(CENTER)

  // background
  fill(vinyl.bgColor)
  noStroke()
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  rect(0, 0, board.w, board.h)
  pop()

  for (var j = vinyl.stepNum; j > 0; j--) {
    push()
    translate(j * 0, j * -board.h * (0.8 / vinyl.stepNum) * 0.5 * vinyl.groupH)
    stroke(
      (vinyl.mainColor[0] - (j + 1) * (-vinyl.bgColor[0] + vinyl.mainColor[0]) * (1 / (vinyl.stepNum + 1))),
      (vinyl.mainColor[1] - (j + 1) * (-vinyl.bgColor[1] + vinyl.mainColor[1]) * (1 / (vinyl.stepNum + 1))),
      (vinyl.mainColor[2] - (j + 1) * (-vinyl.bgColor[2] + vinyl.mainColor[2]) * (1 / (vinyl.stepNum + 1)))
    )
    noFill()
    strokeWeight(board.w * 0.0125)
    strokeCap(ROUND)
    strokeJoin(ROUND)
    for (var i = 0; i < title.a.length; i++) {
      push()
      translate(windowWidth * 0.5 + (i - title.a.length * 0.5 + 0.5) * board.w * 0.19 + board.w * sin(j * vinyl.wobbliness) * 0.0175, windowHeight * 0.5 - board.h * 0.1125 - vinyl.groupY * board.h * 0.5)
      drawLetter(title.a[i], board.w * 0.15 - board.w * 0.15 * (1 / (vinyl.stepNum - j + 1)))
      pop()
    }
    pop()
  }

  for (var j = vinyl.stepNum; j > 0; j--) {
    push()
    translate(j * 0, j * board.h * (0.8 / vinyl.stepNum) * 0.5 * vinyl.g180H)
    stroke(
      (vinyl.mainColor[0] - (j + 1) * (-vinyl.bgColor[0] + vinyl.mainColor[0]) * (1 / (vinyl.stepNum + 1))),
      (vinyl.mainColor[1] - (j + 1) * (-vinyl.bgColor[1] + vinyl.mainColor[1]) * (1 / (vinyl.stepNum + 1))),
      (vinyl.mainColor[2] - (j + 1) * (-vinyl.bgColor[2] + vinyl.mainColor[2]) * (1 / (vinyl.stepNum + 1)))
    )
    noFill()
    strokeWeight(board.w * 0.0175)
    strokeCap(ROUND)
    strokeJoin(ROUND)
    for (var i = 0; i < title.b.length; i++) {
      push()
      translate(windowWidth * 0.5 + (i - title.b.length * 0.5 + 0.5) * board.w * 0.3 + board.w * sin(j * vinyl.wobbliness) * 0.025, windowHeight * 0.5 + board.h * 0.1125 + vinyl.g180Y * board.h * 0.5)
      drawLetter(title.b[i], board.w * 0.225 - board.w * 0.225 * (1 / (vinyl.stepNum - j + 1)))
      pop()
    }
    pop()
  }

  if (vinyl.gradientState === true) {
    setGradient(windowWidth * 0.5 - board.w * 0.5, windowHeight * 0.5 - board.h * 0.5, board.w, board.h, c1, c2, Y_AXIS)
  }
  setNoise()

  // masking
  fill(0)
  stroke(0)
  strokeWeight(1)
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5 + board.h * 0.5 + 0.5 * (windowHeight - board.h) * 0.5)
  rect(0, 0, windowWidth, (windowHeight - board.h) * 0.5)
  pop()
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5 - board.h * 0.5 - 0.5 * (windowHeight - board.h) * 0.5)
  rect(0, 0, windowWidth, (windowHeight - board.h) * 0.5)
  pop()
  push()
  translate(windowWidth * 0.5 - board.w * 0.5 - 0.5 * (windowWidth - board.w) * 0.5, windowHeight * 0.5)
  rect(0, 0, (windowWidth - board.w) * 0.5, windowHeight)
  pop()
  push()
  translate(windowWidth * 0.5 + board.w * 0.5 + 0.5 * (windowWidth - board.w) * 0.5, windowHeight * 0.5)
  rect(0, 0, (windowWidth - board.w) * 0.5, windowHeight)
  pop()
}

function drawLetter(l, s) {
  if (l === '1') {
    rect(0.0625 * s, 0, 0.875 * s, s, 0.03125 * s, 0.25 * s, 0.03125 * s, 0.03125 * s)
    push()
    rect(0, -0.25 * s, s, 0.5 * s, 0.03125 * s, 0.25 * s, 0.03125 * s, 0.03125 * s)
    pop()
  }
  if (l === '8') {
    rect(0, -0.25 * s, s, 0.5 * s, 0.125 * s)
    rect(0, 0.25 * s, s, 0.5 * s, 0.125 * s)
  }
  if (l === '0') {
    rect(0, 0, s, s, 0.125 * s)
  }
  if (l === 'g') {
    arc(0, 0, s, s, 0, Math.PI, CHORD)
    arc(0, 0, s, s, -1.25 * Math.PI, -0.25 * Math.PI, CHORD)
  }
  if (l === 'r') {
    beginShape()
    vertex(-0.5 * s, -0.5 * s)
    vertex(0.5 * s, 0.5 * s)
    vertex(-0.5 * s, 0.5 * s)
    endShape(CLOSE)
    rect(-(1 / 6) * s, -(1 / 6) * s, (2 / 3) * s, (2 / 3) * s)
    arc((1 / 6) * s, -(1 / 6) * s, (2 / 3) * s, (2 / 3) * s, -0.5 * Math.PI, 0.5 * Math.PI)
  }
  if (l === 'o') {
    ellipse(0, 0, s)
    ellipse(0, 0, 0.5 * s)
  }
  if (l === 'u') {
    arc(0, 0, s, s, 0, Math.PI)
    arc(0, 0, s, s, 0, Math.PI)
    rect(0, -0.25 * s, s, 0.5 * s)
  }
  if (l === 'p') {
    rect(-0.25 * s, 0, 0.5 * s, s)
    rect(-0.25 * s, -(1 / 6) * s, 0.5 * s, (2 / 3) * s)
    rect((1 / 12) * s, -(1 / 6) * s, (1 / 6) * s, (2 / 3) * s)
    arc((1 / 6) * s, -(1 / 6) * s, (2 / 3) * s, (2 / 3) * s, -0.5 * Math.PI, 0.5 * Math.PI)
  }
}

function setGradient(x, y, w, h, c1, c2, axis) {
  noFill()

  if (axis === Y_AXIS) {
    // top to bottom gradient
    for (let i = y; i <= y + h; i++) {
      let inter = map(i, y, y + h, 0, 1)
      let c = lerpColor(c1, c2, inter)
      stroke(c)
      line(x, i, x + w, i)
    }
  } else if (axis === X_AXIS) {
    // left to right gradient
    for (let i = x; i <= x + w; i++) {
      let inter = map(i, x, x + w, 0, 1)
      let c = lerpColor(c1, c2, inter)
      stroke(c)
      line(i, y, i, y + h)
    }
  }
}

function setNoise() {
  loadPixels()
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      if (random(1) > 1 - vinyl.noiseAmount) {
        const index = (x + y * width) * 4
        pixels[index] = vinyl.noiseColor[0]
        pixels[index + 1] = vinyl.noiseColor[1]
        pixels[index + 2] = vinyl.noiseColor[2]
      }
    }
  }
  updatePixels()
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)

  // define vinyl size
  if (windowWidth >= windowHeight) {
    board.h = windowHeight * 0.95
    board.w = board.h
  } else {
    board.w = windowWidth * 0.95
    board.h = board.w
  }
}
